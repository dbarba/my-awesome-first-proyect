<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionConsulta1a(){
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()->select("edad")->distinct(),/*(quitar coma)->where(""),*/
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta1(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas (sin repetidos)",
            "sql"=>"SELECT DISTINCT edad FROM ciclista",
        ]);   
    }
    
    public function actionConsulta2a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where('nomequipo = "Artiach"'),
        ]);
        /*:: Operador de resolución de ambito*/
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach'",
        ]);
    }
    
    public function actionConsulta2(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach"',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = Artiach",
        ]);   
    }
    
    public function actionConsulta3a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("edad")->distinct()->where('nomequipo = "Artiach" OR nomequipo = "Amore Vita"'),
                /*['nomequipo'=>["Artiach","Amore Vita"]]*/
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["edad"],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Listar las edad de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    
    public function actionConsulta3(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT edad FROM ciclista WHERE nomequipo = "Artiach" OR nomequipo = "Amore Vita"',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Listar las edades de los ciclistas de Artiach o Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);   
    }
    
    public function actionConsulta4a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct()->where('edad < 25 OR edad > 30'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
        ]);
    }
    
    public function actionConsulta4(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad < 25 OR edad > 30",
        ]);   
    }
    
    public function actionConsulta5a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("dorsal")->distinct()->where('edad >= 28 AND edad <= 32 AND nomequipo = "Banesto"'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad >= 28 AND edad <= 32 AND nomequipo = 'Banesto'",
        ]);
    }
    
    public function actionConsulta5(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT dorsal FROM ciclista WHERE edad >= 28 AND edad <= 32 AND nomequipo = "Banesto"',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
            "sql"=>"SELECT DISTINCT dorsal FROM ciclista WHERE edad >= 28 AND edad <= 32 AND nomequipo = 'Banesto'",
        ]);   
    }
    
    public function actionConsulta6a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre")->distinct()->where('length(nombre) > 8'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nombre"],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE length(nombre) > 8",
        ]);
    }
    
    public function actionConsulta6(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT nombre FROM ciclista WHERE length(nombre) > 8',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE length(nombre) > 8",
        ]);   
    }
    
    public function actionConsulta7a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("nombre, dorsal, upper(nombre) AS 'nombre_mayúscula'")->distinct(),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nombre", "dorsal", "nombre_mayúscula"],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT nombre, dorsal, upper(nombre) AS 'Nombre mayúscula' FROM ciclista",
        ]);
    }
    
    public function actionConsulta7(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT nombre, dorsal, upper(nombre) AS "Nombre mayúscula" FROM ciclista',
        ]);
            
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'dorsal', 'Nombre mayúscula'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql"=>"SELECT DISTINCT nombre, dorsal, upper(nombre) AS 'Nombre mayúscula' FROM ciclista",
        ]);   
    }
    
    public function actionConsulta8a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Lleva::find()->select("dorsal")->distinct()->where('código="MGE"'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);
    }
    
    public function actionConsulta8(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT dorsal FROM lleva WHERE código = "MGE"',
        ]);
            
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);   
    }
    
    public function actionConsulta9a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("nompuerto")->distinct()->where('altura > 1500'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nompuerto"],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500",
        ]);
    }
    
    public function actionConsulta9(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500',
        ]);
            
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nompuerto"],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql"=>"SELECT DISTINCT nompuerto FROM puerto WHERE altura > 1500",
        ]);   
    }
    
    public function actionConsulta10a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where('pendiente > 8 OR altura >= 1800 AND altura <= 3000'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura >= 1800 AND altura <= 3000",
        ]);
    }
    
    public function actionConsulta10(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura >= 1800 AND altura <= 3000',
        ]);
            
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 OR altura >= 1800 AND altura <= 3000",
        ]);   
    }
    
    public function actionConsulta11a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select("dorsal")->distinct()->where('pendiente > 8 AND altura >= 1800 AND altura <= 3000'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 7 cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura >= 1800 AND altura <= 3000",
        ]);
    }
    
    public function actionConsulta11(){
        $dataProvider = new SqlDataProvider ([
        "sql"=>'SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura >= 1800 AND altura <= 3000',
        ]);
            
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 11 con DAO",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT dorsal FROM puerto WHERE pendiente > 8 AND altura >= 1800 AND altura <= 3000",
        ]);   
    }
    /*Ejercicio 2 de Consultas*/
    
    public function actionConsulta12a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) AS total")->distinct(),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["total"],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(*) AS total FROM ciclista",
        ]);
    }
    
    public function actionConsulta12(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT count(*) AS total FROM ciclista',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["total"],
            "titulo"=>"Consulta 12 con DAO",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT count(*) AS total FROM ciclista",
        ]);
    }
    
    public function actionConsulta13a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select("count(*) AS total")->distinct()->where('nomequipo = "Banesto"'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["total"],
            "titulo"=>"Consulta 13 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>'SELECT count(*) AS total FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
    
    public function actionConsulta13(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT count(*) AS total FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["total"],
            "titulo"=>"Consulta 13 con DAO",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>'SELECT count(*) AS total FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
    
    public function actionConsulta14a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('avg(edad) AS "edad_media"')->distinct(),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["edad_media"],
            "titulo"=>"Consulta 14 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>'SELECT avg(edad) AS "Edad Media" FROM ciclista',
        ]);
    }
    
    public function actionConsulta14(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT avg(edad) AS "Edad Media" FROM ciclista',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["Edad Media"],
            "titulo"=>"Consulta 14 con DAO",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>'SELECT avg(edad) AS "Edad Media" FROM ciclista',
        ]);
    }
    
    public function actionConsulta15a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('avg(edad) AS "edad_media"')->distinct()->where('nomequipo = "Banesto"'),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["edad_media"],
            "titulo"=>"Consulta 15 con Active Record",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>'SELECT avg(edad) AS "Edad Media" FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
    
    public function actionConsulta15(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT avg(edad) AS "Edad Media" FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["Edad Media"],
            "titulo"=>"Consulta 15 con DAO",
            "enunciado"=>"La edad media de los del equipo Banesto",
            "sql"=>'SELECT avg(edad) AS "Edad Media" FROM ciclista WHERE nomequipo = "Banesto"',
        ]);
    }
    
    public function actionConsulta16a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nomequipo, avg(edad) AS "edad_media"')->distinct()->groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo", "edad_media"],
            "titulo"=>"Consulta 16 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>'SELECT nomequipo, avg(edad) AS "Edad Media" FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
    public function actionConsulta16(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT nomequipo, avg(edad) AS "Edad Media" FROM ciclista GROUP BY nomequipo',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo", "Edad Media"],
            "titulo"=>"Consulta 16 con DAO",
            "enunciado"=>"La edad media de los ciclistas por cada equipo",
            "sql"=>'SELECT nomequipo, avg(edad) AS "Edad Media" FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
    public function actionConsulta17a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nomequipo, count(*) AS "total"')->distinct()->groupBy("nomequipo"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo", "total"],
            "titulo"=>"Consulta 17 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>'SELECT nomequipo, count(*) AS "total" FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
    public function actionConsulta17(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT nomequipo, count(*) AS "Total" FROM ciclista GROUP BY nomequipo',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo", "Total"],
            "titulo"=>"Consulta 17 con DAO",
            "enunciado"=>"El número de ciclistas por equipo",
            "sql"=>'SELECT nomequipo, count(*) AS "total" FROM ciclista GROUP BY nomequipo',
        ]);
    }
    
    public function actionConsulta18a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select('count(*) AS "total"')->distinct(),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["total"],
            "titulo"=>"Consulta 18 con Active Record",
            "enunciado"=>"El número total de puertos",
            "sql"=>'SELECT count(*) AS "total" FROM puerto',
        ]);
    }
    
    public function actionConsulta18(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT count(*) AS "Total" FROM puerto',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["Total"],
            "titulo"=>"Consulta 18 con DAO",
            "enunciado"=>"El número total de puertos",
            "sql"=>'SELECT count(*) AS "total" FROM puerto',
        ]);
    }
    
    public function actionConsulta19a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find()->select('count(*) AS "total"')->where("altura > 1500"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["total"],
            "titulo"=>"Consulta 19 con Active Record",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>'SELECT count(*) AS "total" FROM puerto WHERE altura > 1500',
        ]);
    }
    
    public function actionConsulta19(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT count(*) AS "Total" FROM puerto WHERE altura > 1500',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["Total"],
            "titulo"=>"Consulta 19 con DAO",
            "enunciado"=>"El número total de puertos mayores de 1500",
            "sql"=>'SELECT count(*) AS "total" FROM puerto WHERE altura > 1500',
        ]);
    }
    
    public function actionConsulta20a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nomequipo')->distinct()->groupBy("nomequipo")->having("count(*) > 4"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo"],
            "titulo"=>"Consulta 20 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>'SELECT DISTINCT nomequipo FROM ciclista GROUP BY nomequipo HAVING (count(*) > 4)',
        ]);
    }
    
    public function actionConsulta20(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT DISTINCT nomequipo FROM ciclista GROUP BY nomequipo HAVING (count(*) > 4)',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo"],
            "titulo"=>"Consulta 20 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",
            "sql"=>'SELECT DISTINCT nomequipo FROM ciclista GROUP BY nomequipo HAVING (count(*) > 4)',
        ]);
    }
    
    public function actionConsulta21a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()->select('nomequipo')->distinct()->where("edad >= 28 AND edad <= 32")->groupBy("nomequipo")->having("count(*) > 4"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo"],
            "titulo"=>"Consulta 21 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>'SELECT DISTINCT nomequipo FROM ciclista WHERE edad >= 28 AND edad <= 32 GROUP BY nomequipo HAVING (count(*) > 4)',
        ]);
    }
    
    public function actionConsulta21(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT DISTINCT nomequipo FROM ciclista WHERE edad >= 28 AND edad <= 32 GROUP BY nomequipo HAVING (count(*) > 4)',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["nomequipo"],
            "titulo"=>"Consulta 21 con DAO",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",
            "sql"=>'SELECT DISTINCT nomequipo FROM ciclista WHERE edad >= 28 AND edad <= 32 GROUP BY nomequipo HAVING (count(*) > 4)',
        ]);
    }
    
    public function actionConsulta22a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select('dorsal, count(*) AS total')->distinct()->groupBy("dorsal"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal", "total"],
            "titulo"=>"Consulta 22 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>'SELECT DISTINCT dorsal, count(*) AS Total FROM etapa GROUP BY dorsal',
        ]);
    }
    
    public function actionConsulta22(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT DISTINCT dorsal, count(*) AS Total FROM etapa GROUP BY dorsal',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal", "Total"],
            "titulo"=>"Consulta 22 con DAO",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas",
            "sql"=>'SELECT DISTINCT dorsal, count(*) AS Total FROM etapa GROUP BY dorsal',
        ]);
    }
    
    public function actionConsulta23a(){
        $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()->select('dorsal')->distinct()->groupBy("dorsal")->having("count(*) > 1"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 23 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>'SELECT DISTINCT dorsal FROM etapa GROUP BY dorsal HAVING (count(*) > 1)',
        ]);
    }
    
    public function actionConsulta23(){
        $dataProvider = new SqlDataProvider([
            "sql"=>'SELECT DISTINCT dorsal FROM etapa GROUP BY dorsal HAVING (count(*) > 1)',
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>["dorsal"],
            "titulo"=>"Consulta 23 con DAO",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>'SELECT DISTINCT dorsal FROM etapa GROUP BY dorsal HAVING (count(*) > 1)',
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
