<?php
use yii\helpers\Html;
/** @var yii\web\View $this */

$this->title = 'Consultas de selección 1';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de selección</h1>
    </div>

    <div class="body-content">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta1a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta1'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta2a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta2'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o AmoreVita</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta3a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta3'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 4</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta4a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta4'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 5</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta5a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta5'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 6</h3>
                        <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta6a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta6'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 7</h3>
                        <p>Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta7a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta7'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 8</h3>
                        <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta8a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta8'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los puertos cuya altura sea mayor de 1500</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta9a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta9'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 10</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta10a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta10'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 11</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta11a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta11'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 12</h3>
                        <p>Número de ciclistas que hay</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta12a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta12'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 13</h3>
                        <p>Número de ciclistas que hay del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta13a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta13'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 14</h3>
                        <p>Edad media de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta14a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta14'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 15</h3>
                        <p>La edad media de los del equipo Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta15a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta15'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 16</h3>
                        <p>La edad media de los ciclistas por cada equipo</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta16a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta16'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 17</h3>
                        <p>El número de ciclistas por equipo</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta17a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta17'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 18</h3>
                        <p>El número total de puertoso</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta18a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta18'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 19</h3>
                        <p>El número total de puertos mayores de 1500</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta19a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta19'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 20</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta20a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta20'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 21</h3>
                        <p>Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta21a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta21'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 22</h3>
                        <p>Indícame el número de etapas que ha ganado cada uno de los ciclistas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta22a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta22'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 23</h3>
                        <p>Indícame el dorsal de los ciclistas que hayan ganado más de una etapa</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta23a'],['class'=>'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta23'],['class'=>'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
